'''The task is to create an application that takes a single integer input. The
application will download the raw data from an json file (playerlist.json)
and print a list of all pairs of players whose height in inches adds up to
the integer input to the application. If no matches are found, the application will print "No matches found"

Sample output is as follows:
```
> app 139

- Brevin Knight Nate Robinson
- Nate Robinson Mike Wilks
```

The algorithm to find the pairs must be faster than O(n^2). All edge cases
should be handled appropriately. Though not strictly required, demonstrating
comfort in writing unit tests will make your submission stand out. This is
_not_ a closed book test. You are encouraged to reach out with any questions
that you come across.'''
import json


def file():
    with open('playerlist.json') as file:
        data = json.load(file)
        h_in = list()
        names = list()

        for i in data['values']:
            h_in.append(int(i['h_in']))
            names.append(i['first_name'] + ' ' + i['last_name'])

    return h_in, names


def search(number, h_in):
    print(h_in)
    print(number)

    index = {}
    for i, n in enumerate(h_in):
        if number - n in index:
            return index[number - n], i

        index[n] = i


if __name__ == '__main__':
    h_in, names = file()
    result = search(number=154, h_in=h_in)
    result2 = names[result[0]]
    result3 = names[result[1]]

    print(result)
    print(result2)
    print(result3)


# 1. input
